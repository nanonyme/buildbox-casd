/*
 * Copyright 2019 Bloomberg Finance LP
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <buildboxcasd_hardlinkstager.h>

#include <buildboxcasd_fslocalcas.h>

#include <buildboxcommon_protos.h>
#include <buildboxcommon_temporarydirectory.h>

#include <dirent.h>
#include <fstream>

#include <gtest/gtest.h>

using namespace buildboxcasd;
using buildboxcommon::Directory;
using buildboxcommon::DirectoryNode;
using buildboxcommon::FileNode;
using buildboxcommon::SymlinkNode;

class FileStagerFixture : public ::testing::Test {
  protected:
    FileStagerFixture()
        : cas_root_directory(), cas(cas_root_directory.name()), stager(&cas)
    {
        // Adding two files to the cas...
        file_contents["file1.sh"] = "file1Contents...";
        file_digests["file1.sh"] = make_digest(file_contents["file1.sh"]);
        cas.writeBlob(file_digests["file1.sh"], file_contents["file1.sh"]);

        file_contents["file2.c"] = "file2: [...data...]";
        file_digests["file2.c"] = make_digest(file_contents["file2.c"]);
        cas.writeBlob(file_digests["file2.c"], file_contents["file2.c"]);

        // ... and creating a directory structure that contains them:
        createDirectoryTree();
    }

    void createDirectoryTree()
    {
        /*
         *  root/
         *  |-- file1.sh*
         *  |-- link -> subdir1/file2.c
         *  |-- subdir1/
         *           |-- file2.c
         */

        // Creating `root/`:
        Directory root_directory;

        // Adding a file to it:
        FileNode *f1 = root_directory.add_files();
        f1->set_name("file1.sh");
        f1->set_is_executable(true);
        f1->mutable_digest()->CopyFrom(file_digests["file1.sh"]);

        // Adding symlink (`link` -> `subdir1/file2.c`)
        SymlinkNode *link = root_directory.add_symlinks();
        link->set_name("link");
        link->set_target("subdir1/file2.c");

        // Creating `subdir1/`:
        Directory subdirectory;

        // Adding `subdir1/file2.c`:
        FileNode *f2 = subdirectory.add_files();
        f2->set_name("file2.c");
        f2->mutable_digest()->CopyFrom(file_digests["file2.c"]);

        // Adding `subdir1/` under `dirA/`:
        DirectoryNode *d1 = root_directory.add_directories();
        d1->set_name("subdir1");

        const auto serialized_subdirectory = subdirectory.SerializeAsString();
        const auto subdirectory_digest = make_digest(serialized_subdirectory);
        d1->mutable_digest()->CopyFrom(subdirectory_digest);

        const auto serialized_root_directory =
            root_directory.SerializeAsString();
        const auto root_directory_digest =
            make_digest(serialized_root_directory);

        // Storing the two Directory protos...
        cas.writeBlob(root_directory_digest, serialized_root_directory);
        cas.writeBlob(subdirectory_digest, serialized_subdirectory);

        this->root_directory_digest = root_directory_digest;
    }

    inline static Digest make_digest(const std::string &data)
    {
        return buildboxcommon::CASHash::hash(data);
    }

    Digest root_directory_digest;

    std::map<std::string, Digest> file_digests;
    std::map<std::string, std::string> file_contents;

    buildboxcommon::TemporaryDirectory cas_root_directory;
    FsLocalCas cas;
    FileStager stager;

    buildboxcommon::TemporaryDirectory stage_directory;
};

TEST_F(FileStagerFixture, StageDirectoryNotEmptyThrows)
{
    const std::string stage_directory_path(stage_directory.name());

    const auto file_path = stage_directory_path + "/file.txt";
    std::ofstream f(file_path.c_str());
    f.close();
    ASSERT_TRUE(f.good());

    ASSERT_THROW(stager.stage(root_directory_digest, stage_directory_path),
                 std::invalid_argument);
}

TEST_F(FileStagerFixture, Stage)
{
    const std::string stage_path = stage_directory.name();

    stager.stage(root_directory_digest, stage_directory.name());

    DIR *dir = opendir(stage_directory.name());
    ASSERT_NE(dir, nullptr);

    // The staged directory contains the relevant contents and nothing more:
    struct dirent *entry;
    errno = 0;
    while ((entry = readdir(dir)) != nullptr) {
        const auto entry_name = std::string(entry->d_name);

        if (entry_name == "." || entry_name == "..") {
            continue;
        }
        else if (entry_name == "file1.sh"
#ifdef _DIRENT_HAVE_D_TYPE
                 && (entry->d_type == DT_REG)
#endif
        ) {
            continue;
        }
        else if (entry_name == "subdir1"
#ifdef _DIRENT_HAVE_D_TYPE
                 && entry->d_type == DT_DIR
#endif
        ) {
            continue;
        }
        else if (entry_name == "link"
#ifdef _DIRENT_HAVE_D_TYPE
                 && entry->d_type == DT_LNK
#endif
        ) {
            continue;
        }
        else {
            ASSERT_TRUE(false);
        }
    }

    // `readdir()` did not fail; we scanned the whole stage directory:
    ASSERT_EQ(errno, 0);

    /*
     * file1.c
     */
    // Its contents match:
    const std::string file1_stage_path = stage_path + "/file1.sh";
    std::ifstream f1(file1_stage_path, std::fstream::binary);
    ASSERT_TRUE(f1.good());

    std::stringstream file1_contents;
    file1_contents << f1.rdbuf();
    ASSERT_TRUE(f1.good());
    ASSERT_EQ(file1_contents.str(), file_contents["file1.sh"]);

    // And it also has +x:
    struct stat file1_stat;
    ASSERT_EQ(stat(file1_stage_path.c_str(), &file1_stat), 0);
    ASSERT_TRUE(file1_stat.st_mode & S_IXUSR);

    /*
     * subdir/file2.c
     */
    // Its contents match:
    const std::string file2_stage_path = stage_path + "/subdir1/file2.c";
    std::ifstream f2(file2_stage_path, std::fstream::binary);
    std::stringstream file2_contents;
    file2_contents << f2.rdbuf();
    ASSERT_EQ(file2_contents.str(), file_contents["file2.c"]);

    // But it is *not* executable:
    struct stat file2_stat;
    ASSERT_EQ(stat(file2_stage_path.c_str(), &file2_stat), 0);
    ASSERT_FALSE(file2_stat.st_mode & S_IXUSR);

    /*
     * link -> subdir/file2.c
     */
    const std::string link_stage_path = stage_path + "/link";
    std::ifstream f3(link_stage_path, std::fstream::binary);
    std::stringstream link_target_contents;
    link_target_contents << f3.rdbuf();
    ASSERT_EQ(link_target_contents.str(), file_contents["file2.c"]);
}

TEST_F(FileStagerFixture, UnstageDeletingStageDir)
{
    buildboxcommon::TemporaryDirectory stage_dir;
    stage_dir.setAutoRemove(false);

    stager.stage(root_directory_digest, stage_dir.name());

    ASSERT_FALSE(
        buildboxcommon::FileUtils::directoryIsEmpty(stage_dir.name()));
    // Unstage and delete the top-level directory too:
    stager.unstage(stage_dir.name(), true);
    ASSERT_FALSE(buildboxcommon::FileUtils::isDirectory(stage_dir.name()));
}

TEST_F(FileStagerFixture, UnstageClearingStageDir)
{
    buildboxcommon::TemporaryDirectory stage_dir;
    stager.stage(root_directory_digest, stage_dir.name());
    ASSERT_FALSE(
        buildboxcommon::FileUtils::directoryIsEmpty(stage_dir.name()));

    // Unstage but do not delete the root directory (default):
    stager.unstage(stage_dir.name());

    ASSERT_TRUE(buildboxcommon::FileUtils::isDirectory(stage_dir.name()));
    ASSERT_TRUE(buildboxcommon::FileUtils::directoryIsEmpty(stage_dir.name()));
}

TEST_F(FileStagerFixture, SymlinkWithDoubleSlashThrows)
{
    Directory directory;
    SymlinkNode *symlink = directory.add_symlinks();

    symlink->set_name("linkToAbsolutePath");
    symlink->set_target("//is/not/allowed");

    const auto serialized_directory = directory.SerializeAsString();
    const Digest directory_digest = make_digest(serialized_directory);

    cas.writeBlob(directory_digest, serialized_directory);

    ASSERT_THROW(stager.stage(directory_digest, stage_directory.name()),
                 std::invalid_argument);
}

TEST_F(FileStagerFixture, SymlinkWithSlashDotSlashThrows)
{
    Directory directory;
    SymlinkNode *symlink = directory.add_symlinks();

    symlink->set_name("linkToAbsolutePath");
    symlink->set_target("/not/./allowed");

    const auto serialized_directory = directory.SerializeAsString();
    const Digest directory_digest = make_digest(serialized_directory);

    cas.writeBlob(directory_digest, serialized_directory);

    ASSERT_THROW(stager.stage(directory_digest, stage_directory.name()),
                 std::invalid_argument);
}
