cmake_minimum_required(VERSION 3.6)
set(CMAKE_CXX_STANDARD 17)

project(buildbox-casd C CXX)

set(CMAKE_THREAD_PREFER_PTHREAD TRUE)
find_package(Threads REQUIRED)
find_package(BuildboxCommon REQUIRED)

find_package(gRPC)
if(gRPC_FOUND)
    set(GRPC_TARGET gRPC::grpc++)
    set(GRPC_CPP_PLUGIN $<TARGET_FILE:gRPC::grpc_cpp_plugin>)
else()
    find_package(PkgConfig REQUIRED)
    pkg_check_modules(gRPC REQUIRED IMPORTED_TARGET grpc++)
    set(GRPC_TARGET PkgConfig::gRPC)
    find_program(GRPC_CPP_PLUGIN grpc_cpp_plugin)
endif()

if(NOT DEFINED PROTOBUF_TARGET)
    message(FATAL_ERROR "PROTOBUF_TARGET undefined, should be from BuildboxCommon.")
endif()

if(NOT BUILDBOX_CASD_VERSION)
    find_program(GIT git)

    if(GIT STREQUAL "GIT-NOTFOUND")
        set(GIT_SHA "unknown")
        message("Could not find git, setting version to 'unknown'")
    elseif(NOT EXISTS "${CMAKE_CURRENT_SOURCE_DIR}/.git")
        set(GIT_SHA "unknown")
        message("Not in a git repository, setting version to 'unknown'")
    else()
        execute_process(
            COMMAND "git" "describe" "--tags" "--long"
            WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
	    OUTPUT_VARIABLE GIT_SHA
            OUTPUT_STRIP_TRAILING_WHITESPACE)
        message(STATUS "Using git commit as version: ${GIT_SHA}")
    endif()
    set(BUILDBOX_CASD_VERSION ${GIT_SHA})
endif()

add_definitions("-DBUILDBOX_CASD_VERSION=\"${BUILDBOX_CASD_VERSION}\"")

option(CAS_SERVER_TESTS_USES_TCP "CAS server tests use tcp" OFF)
if (CAS_SERVER_TESTS_USES_TCP)
    add_definitions("-DUSER_TEST_PROXY_SERVER_ADDRESS=\"127.0.0.1:50055\"")
    add_definitions("-DUSER_TEST_REMOTE_SERVER_ADDRESS=\"127.0.0.1:50065\"")
else()
    add_definitions("-DUSER_TEST_PROXY_SERVER_ADDRESS=\"\"")
    add_definitions("-DUSER_TEST_REMOTE_SERVER_ADDRESS=\"\"")
endif()

add_subdirectory(buildboxcasd)

include(CTest)
if(BUILD_TESTING)
    add_subdirectory(test)
endif()

option(BUILD_BENCHMARK "Build benchmarks" ON)
if(BUILD_BENCHMARK)
    add_subdirectory(benchmark)
endif()
