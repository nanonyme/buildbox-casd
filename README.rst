What is ``buildbox-casd``?
==========================

``buildbox-casd`` is a long-lived local cache and proxy for the
``ContentAddressableStorage`` services described by Bazel's
`Remote Execution API`_. Its intended purpose is to run on the same host
as a remote worker or client to provide fast access to data in CAS.

.. _Remote Execution API: https://docs.google.com/document/d/1AaGk7fOPByEvpAbqeXIyE8HX_A3_axxNnvroblTZ_6s

It also supports the `LocalCAS Protocol`_.

.. _LocalCAS Protocol: https://gitlab.com/BuildGrid/buildbox/buildbox-common/blob/master/protos/build/buildgrid/local_cas.proto


Installation
============

``buildbox-casd`` requires only `buildbox-common`_ as a dependency. You can install it anywhere
on your system. If you install it system-wide, you can omit the ``-DBuildboxCommon_DIR`` flag
below.

Once you have installed ``buildbox-common`` somewhere, you can build ``buildbox-casd`` as follows:

.. code-block:: sh

    git clone https://gitlab.com/BuildGrid/buildbox/buildbox-casd.git && cd buildbox-casd
    mkdir build && cd build
    cmake -DBuildboxCommon_DIR=/path/to/installation/usr/local/lib/cmake/BuildboxCommon/ ..
    make

You can then install it in the current environment with

.. code-block:: sh

    make install

.. _buildbox-common: https://gitlab.com/BuildGrid/buildbox/buildbox-common


Usage
=====

``buildbox-casd`` can be configured in two modes:

* As a regular CAS server that stores blobs on disk. In this mode, ``buildbox-casd`` is the source of truth for all blobs in CAS.

* As a CAS proxy. In this mode ``buildbox-casd`` is pointed to a remote CAS server, and it will forward all requests to it while caching data locally.

   buildbox-casd [OPTIONS] LOCAL_CACHE_PATH

This starts a daemon storing local cached data at the ``LOCAL_CACHE_PATH`` path.

The key parameters are:

* ``--cas-remote=URL``

  URL of the remote CAS server to which requests should be forwarded to.
  Setting this option configures ``buildbox-casd`` to act in proxy mode.

  If this is not provided, ``buildbox-casd`` behaves like a regular CAS server storing blobs on disk.

  If this is provided, an additional server instance can be created with ``--server-instance=NAME``.

* ``--ra-remote=URL``

  URL of the Remote Asset server to which requests should be forwarded to
  Setting this option configures ``buildbox-casd`` to act in the proxy mode.

  If this is not provided, ``buildbox-casd`` behaves like a regular Remote Asset server storing blobs on disk.

* ``--instance=NAME``

  Name given to the new LocalCAS instance (if not set, an empty string).

* ``--verbose``

  Output additional information to stdout that may be useful when debugging.

* ``--bind=URL``

  Endpoint where ``casd`` listens for incoming requests.

  By default a Unix domain socket in ``LOCAL_CACHE_PATH/casd.sock``.

* ``--metrics-mode=MODE``

  Where mode is one of the following: ``udp://<hostname>:<port>``, ``file:///path/to/file``, or ``stderr``

* ``--metrics-publish-interval=VALUE``

  Where VALUE is the number of seconds that the background thread sleeps in between publishing metrics


Full usage
----------
::

    Usage: ./buildboxcasd/buildbox-casd
        --instance                       Name of instance [optional, default = ""]
        --cas-remote                     URL for the CAS service [optional]
        --cas-instance                   Name of the CAS instance [optional, default = ""]
        --cas-server-cert                Public server certificate for CAS TLS (PEM-encoded) [optional]
        --cas-client-key                 Private client key for CAS TLS (PEM-encoded) [optional]
        --cas-client-cert                Private client certificate for CAS TLS (PEM-encoded) [optional]
        --cas-access-token               Access Token for authentication CAS (e.g. JWT, OAuth access token, etc), will be included as an HTTP Authorization bearer token [optional]
        --cas-token-reload-interval      How long to wait before refreshing access token [optional]
        --cas-googleapi-auth             Use GoogleAPIAuth for CAS service [optional, default = false]
        --cas-retry-limit                Number of times to retry on grpc errors for CAS service [optional, default = "4"]
        --cas-retry-delay                How long to wait in milliseconds before the first grpc retry for CAS service [optional, default = "1000"]
        --cas-request-timeout            Sets the timeout for gRPC requests in seconds. Set to 0 for no timeout. [optional, default = "0"]
        --cas-min-throughput             Sets the minimum throughput for gRPC requests in bytes per seconds.
                                            The value may be suffixed with K, M, G or T. [optional, default = "0"]
        --cas-keepalive-time             Sets the period for gRPC keepalive pings in seconds. Set to 0 to disable keepalive pings. [optional, default = "0"]
        --cas-load-balancing-policy      Which grpc load balancing policy to use for CAS service.
                                            Valid options are 'round_robin' and 'grpclb' [optional]
        --server-instance                Create an additional local CAS server instance (useful when --cas-remote is specified). [optional]
        --ac-remote                      URL for the Action Cache service [optional]
        --ac-instance                    Name of the Action Cache instance [optional, default = ""]
        --ac-server-cert                 Public server certificate for Action Cache TLS (PEM-encoded) [optional]
        --ac-client-key                  Private client key for Action Cache TLS (PEM-encoded) [optional]
        --ac-client-cert                 Private client certificate for Action Cache TLS (PEM-encoded) [optional]
        --ac-access-token                Access Token for authentication Action Cache (e.g. JWT, OAuth access token, etc), will be included as an HTTP Authorization bearer token [optional]
        --ac-token-reload-interval       How long to wait before refreshing access token [optional]
        --ac-googleapi-auth              Use GoogleAPIAuth for Action Cache service [optional, default = false]
        --ac-retry-limit                 Number of times to retry on grpc errors for Action Cache service [optional, default = "4"]
        --ac-retry-delay                 How long to wait in milliseconds before the first grpc retry for Action Cache service [optional, default = "1000"]
        --ac-request-timeout             Sets the timeout for gRPC requests in seconds. Set to 0 for no timeout. [optional, default = "0"]
        --ac-min-throughput              Sets the minimum throughput for gRPC requests in bytes per seconds.
                                            The value may be suffixed with K, M, G or T. [optional, default = "0"]
        --ac-keepalive-time              Sets the period for gRPC keepalive pings in seconds. Set to 0 to disable keepalive pings. [optional, default = "0"]
        --ac-load-balancing-policy       Which grpc load balancing policy to use for Action Cache service.
                                            Valid options are 'round_robin' and 'grpclb' [optional]
        --ra-remote                      URL for the Remote Asset service [optional]
        --ra-instance                    Name of the Remote Asset instance [optional, default = ""]
        --ra-server-cert                 Public server certificate for Remote Asset TLS (PEM-encoded) [optional]
        --ra-client-key                  Private client key for Remote Asset TLS (PEM-encoded) [optional]
        --ra-client-cert                 Private client certificate for Remote Asset TLS (PEM-encoded) [optional]
        --ra-access-token                Access Token for authentication Remote Asset (e.g. JWT, OAuth access token, etc), will be included as an HTTP Authorization bearer token [optional]
        --ra-token-reload-interval       How long to wait before refreshing access token [optional]
        --ra-googleapi-auth              Use GoogleAPIAuth for Remote Asset service [optional, default = false]
        --ra-retry-limit                 Number of times to retry on grpc errors for Remote Asset service [optional, default = "4"]
        --ra-retry-delay                 How long to wait in milliseconds before the first grpc retry for Remote Asset service [optional, default = "1000"]
        --ra-request-timeout             Sets the timeout for gRPC requests in seconds. Set to 0 for no timeout. [optional, default = "0"]
        --ra-min-throughput              Sets the minimum throughput for gRPC requests in bytes per seconds.
                                            The value may be suffixed with K, M, G or T. [optional, default = "0"]
        --ra-keepalive-time              Sets the period for gRPC keepalive pings in seconds. Set to 0 to disable keepalive pings. [optional, default = "0"]
        --ra-load-balancing-policy       Which grpc load balancing policy to use for Remote Asset service.
                                            Valid options are 'round_robin' and 'grpclb' [optional]
        --exec-remote                    URL for the Execution service [optional]
        --exec-instance                  Name of the Execution instance [optional, default = ""]
        --exec-server-cert               Public server certificate for Execution TLS (PEM-encoded) [optional]
        --exec-client-key                Private client key for Execution TLS (PEM-encoded) [optional]
        --exec-client-cert               Private client certificate for Execution TLS (PEM-encoded) [optional]
        --exec-access-token              Access Token for authentication Execution (e.g. JWT, OAuth access token, etc), will be included as an HTTP Authorization bearer token [optional]
        --exec-token-reload-interval     How long to wait before refreshing access token [optional]
        --exec-googleapi-auth            Use GoogleAPIAuth for Execution service [optional, default = false]
        --exec-retry-limit               Number of times to retry on grpc errors for Execution service [optional, default = "4"]
        --exec-retry-delay               How long to wait in milliseconds before the first grpc retry for Execution service [optional, default = "1000"]
        --exec-request-timeout           Sets the timeout for gRPC requests in seconds. Set to 0 for no timeout. [optional, default = "0"]
        --exec-min-throughput            Sets the minimum throughput for gRPC requests in bytes per seconds.
                                            The value may be suffixed with K, M, G or T. [optional, default = "0"]
        --exec-keepalive-time            Sets the period for gRPC keepalive pings in seconds. Set to 0 to disable keepalive pings. [optional, default = "0"]
        --exec-load-balancing-policy     Which grpc load balancing policy to use for Execution service.
                                            Valid options are 'round_robin' and 'grpclb' [optional]
        --bind                           Bind to address:port or UNIX socket in unix:path [optional]
        --quota-high                     Maximum local cache size (e.g., 50G or 2T) [optional]
        --quota-low                      Local cache size to retain on LRU expiry [optional]
        --reserved                       Reserved disk space (headroom left when `quota_high` gets automatically reduced) [optional, default = "2G"]
        --protect-session-blobs          Do not expire blobs created or used in the current session [optional, default = false]
        --findmissingblobs-cache-ttl     Number of seconds to cache the results of
                                            FindMissingBlobs() calls when in proxy mode [optional, default = 0]
        --capture-allow-file-moves       Attempt to move files into the CAS when a LocalCAS capture request has its `move_files` flag set. This will avoid a copy only when a file is in the same filesystem as the LOCAL_CACHE directory and owned by the user that is running casd. NOTE: Only enable if clients can be trusted to not break the consistency of the storage. [optional, default = false]
        --log-level                      Logging verbosity level [optional, default = "error"]
        --metrics-mode                   Metrics Mode: --metrics-mode=MODE - options for MODE are
                                            udp://<hostname>:<port>
                                            file:///path/to/file
                                            stderr [optional]
        --metrics-publish-interval       Metrics publishing interval [optional, default = 15]
        --verbose                        Set log level to debug [optional]
        --read-only-remote               Specifies if casd should update the remote CAS with local build artifacts [optional]
        --version                        Print version information and exit [optional]
        --help                           Display usage and exit
          Cache path                     POSITIONAL [required]

**Note**: URLs can be of the form ``http(s)://address:port``, ``unix:path`` (with a relative or absolute path), or ``unix://path`` (with an absolute path).

Staging and Capturing
=====================

As mentioned in the introduction, ``buildbox-casd`` supports the `LocalCAS Protocol`_.
This allows you to put files and directories from the remote filesystem onto
your local filesystem (known as "Staging") and snapshot files or a directory structure
for placement into CAS (known as "Capturing"). Using this protocol allows you to
effectively use buildbox-casd as a local filesystem cache for your remote CAS.

`buildbox-worker`_ supports communication with this protocol. To enable usage, pass
:code:`--runner-arg=--use-localcas` on the **buildbox-worker** commandline.

The stage/capture mechanism can be used in two modes: with FUSE and with hardlinks.
The FUSE mode requires `buildbox-fuse`_ to be present on the system, NodeProperties are
supported, and writing to the input files is generally safe. The hardlinks mode does not
require the ``buildbox-fuse`` tool, but NodeProperties are not supported, and writing to
staged files within actions is unsafe since future actions will read the modified files.
FUSE is enabled by default, and if ``buildbox-fuse`` is not on the system or FUSE is not
supported on the system, ``buildbox-casd`` will use hardlinks.

**For staging and capturing to be completely safe in hardlinks mode, please run**
**buildbox-casd as a different user from the client.** If you run them as the same user,
actions sent to the worker in hardlinks mode may corrupt the staged files.

.. _buildbox-worker: https://gitlab.com/BuildGrid/buildbox/buildbox-worker

.. _LocalCAS Protocol: https://gitlab.com/BuildGrid/buildbox/buildbox-common/blob/master/protos/build/buildgrid/local_cas.proto

.. _buildbox-fuse: https://gitlab.com/BuildGrid/buildbox/buildbox-fuse

Metrics
=======
`buildbox-casd` publishes `statsd`_  runtime metrics throughout its lifetime. Documentation listing the names and descriptions of these published metrics can be found with the `MetricName`_ definitions.

.. _statsd: https://github.com/statsd/statsd/blob/master/docs/metric_types.md
.. _MetricName: https://gitlab.com/BuildGrid/buildbox/buildbox-casd/-/blob/master/buildboxcasd/buildboxcasd_metricnames.cpp

Metrics Enablement
------------------
To enable metrics publishing, two command line options are required:
::

   --metrics-mode=MODE                Options for MODE are:
           udp://<hostname>:<port>
           file:///path/to/file
           stderr

   --metrics-publish-interval=VALUE   Publish metric at the specified interval rate in seconds, defaults 15 seconds

Example #1: Write metrics to a statsd server on the local host listening on port 50051 and configure the background publishing thread to publish every 5 seconds
::
   --metrics-mode=udp://localhost:50051 --metrics-publish-interval=5

Example #2: Write metrics to stderr and configure the background publishing thread to publish every 5 seconds
::
   --metrics-mode=stderr --metrics-publish-interval=5

Example #3: Write metrics to a file and configure the background publishing thread to publish every 5 seconds
::
   --metrics-mode=file:///tmp/my-metrics.log --metrics-publish-interval=5
