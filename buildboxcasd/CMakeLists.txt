
file(GLOB SRCS *.cpp)

set (LIBS
    Buildbox::buildboxcommon
    Threads::Threads
    ${PROTOBUF_TARGET}
    ${GRPC_TARGET})

add_library(casd STATIC ${SRCS})
target_link_libraries(casd ${LIBS})
target_include_directories(casd PRIVATE ${CMAKE_CURRENT_SOURCE_DIR})

## Add compiler specific flags for -Werror. PRIVATE specifies to not add these flags to libraries that link against this one (in this case tests).
if(CMAKE_BUILD_TYPE STREQUAL "DEBUG")
    set(DEBUG_FLAGS -Werror -Wextra -pedantic-errors -Wall -Wconversion -Wno-vla)
endif()
if(CMAKE_CXX_COMPILER_ID EQUAL Clang)
    target_compile_options(casd PRIVATE -Wall -Werror=inconsistent-missing-override -Werror=shadow ${DEBUG_FLAGS})
elseif(CMAKE_CXX_COMPILER_ID EQUAL GNU)
    # We want to avoid setting errors on GNU + AIX because system headers + not being able to use -isystem
    if(${CMAKE_SYSTEM_NAME} MATCHES "AIX")
        message("Skipping all warnings due to GNU compiler + AIX system")
	target_link_options(casd PRIVATE -lsupc++)
    else()
        target_compile_options(casd PRIVATE -Wall -Werror=suggest-override -Werror=shadow ${DEBUG_FLAGS})
    endif()
else()
    target_compile_options(casd PRIVATE -Wall ${DEBUG_FLAGS})
endif()

# Before gRPC v1.30.0 a bug prevented clients from being notitied that streams
# were half-closed by the other end: https://github.com/grpc/grpc/pull/22668.
# casd closes its end of a stream when returning early from
# `ByteStream.Write()` calls when the blob is already in storage.
# In order to avoid potential side effects with buggy clients that will try and
# keep sending data to a closed stream, we only enable that early return for
# versions of gRPC that have that issue fixed.
if(gRPC_VERSION VERSION_GREATER_EQUAL 1.30.0 OR BUILDBOX_CASD_BYTESTREAM_WRITE_RETURN_EARLY)
  target_compile_definitions(casd PRIVATE BUILDBOX_CASD_BYTESTREAM_WRITE_RETURN_EARLY)
  message("Enabling BUILDBOX_CASD_BYTESTREAM_WRITE_RETURN_EARLY")
endif()

add_executable(buildbox-casd bin/buildboxcasd.m.cpp)
target_link_libraries(buildbox-casd casd)
target_include_directories(buildbox-casd PRIVATE ${CMAKE_CURRENT_SOURCE_DIR})
install(TARGETS buildbox-casd RUNTIME DESTINATION bin)
