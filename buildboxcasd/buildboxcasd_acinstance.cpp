/*
 * Copyright 2021 Bloomberg Finance LP
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <buildboxcasd_acinstance.h>
#include <buildboxcommon_logging.h>
#include <string>
#include <unordered_set>

using namespace buildboxcasd;
using namespace buildboxcommon;

static void add_directory_to_request(const Directory &d,
                                     FindMissingBlobsRequest *req)
{
    for (const FileNode &f : d.files()) {
        req->add_blob_digests()->CopyFrom(f.digest());
    }
    for (const DirectoryNode &d_node : d.directories()) {
        req->add_blob_digests()->CopyFrom(d_node.digest());
    }
}

bool AcInstance::hasAllDigests(ActionResult *result)
{
    FindMissingBlobsRequest req;
    req.set_instance_name(d_cas->instanceName());
    bool hasMissing = false;

    if (result->has_stdout_digest()) {
        req.add_blob_digests()->CopyFrom(result->stdout_digest());
    }
    if (result->has_stderr_digest()) {
        req.add_blob_digests()->CopyFrom(result->stderr_digest());
    }

    for (const OutputFile &file : result->output_files()) {
        req.add_blob_digests()->CopyFrom(file.digest());
    }

    BatchReadBlobsRequest tree_req;
    BatchReadBlobsResponse tree_res;
    for (const OutputDirectory &out_dir : result->output_directories()) {
        tree_req.add_digests()->CopyFrom(out_dir.tree_digest());
    }

    if (!d_cas->BatchReadBlobs(tree_req, &tree_res).ok()) {
        BUILDBOXCOMMON_THROW_EXCEPTION(
            std::runtime_error,
            "BatchReadBlobs failed in AcInstance::hasAllDigests");
    }

    for (const auto &tree : tree_res.responses()) {
        if (tree.status().code() == grpc::StatusCode::NOT_FOUND) {
            hasMissing = true;
            BUILDBOX_LOG_DEBUG("Tree with digest=" + tree.digest().hash() +
                               " is missing");
            continue;
        }
        else if (tree.status().code() != grpc::StatusCode::OK) {
            BUILDBOXCOMMON_THROW_EXCEPTION(
                std::runtime_error,
                "readBlob failed in AcInstance::hasAllDigests: "
                    << tree.status().code() << ": "
                    << tree.status().message());
        }
        Tree t;
        t.ParseFromString(tree.data());

        add_directory_to_request(t.root(), &req);
        for (const Directory &d :
             t.children()) { // children already recursive,
                             // don't need to go deeper
            add_directory_to_request(d, &req);
        }
    }

    FindMissingBlobsResponse res;

    grpc::Status code = d_cas->FindMissingBlobs(req, &res);

    if (!code.ok()) {
        BUILDBOX_LOG_ERROR(
            "ActionCache unable to check CAS, returning not in CAS");
        return true;
    }
    if (!res.missing_blob_digests().empty()) {
        hasMissing = true;
        BUILDBOX_LOG_INFO("ActionResult missing "
                          << res.missing_blob_digests().size() << " digests");
    }

    return !hasMissing;
}
